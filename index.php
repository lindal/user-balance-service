<?php
use app\App;

require_once 'vendor/autoload.php';

$config = require 'config.php';

$app = new App($config);
$app->run();