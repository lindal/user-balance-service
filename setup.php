<?php
$config = require 'config.php';

$pdo = new PDO(
    $config['pdo']['dsn'],
    $config['pdo']['user'],
    $config['pdo']['password']
);

$sql = "CREATE TABLE IF NOT EXISTS `dev`.`transaction` ( `id` INT NOT NULL AUTO_INCREMENT , `user_id` INT NOT NULL , `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `type` INT NOT NULL , `value` INT NOT NULL , `status` INT NOT NULL , PRIMARY KEY (`id`), INDEX (`user_id`)) ENGINE = InnoDB;";

$stmt = $pdo->prepare($sql);
$stmt->execute();