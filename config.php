<?php
return [
    'broker' => [
        'host' => 'rabbitMq',
        'port' => 5672,
        'user' => 'rabbitmq',
        'password' => 'rabbitmq',
        'queue' => 'rpc_queue'
    ],
    'pdo' => [
        'dsn' => 'mysql:host=db;dbname=dev;charset=utf8',
        'user' => 'root',
        'password' => '123456'
    ],
    'availableTransactionTypes' => [
        'debit' => \app\transactions\Debit::class,
        'credit' => \app\transactions\Credit::class,
        'userToUser' => \app\transactions\UserToUser::class,
        'block' => \app\transactions\Block::class,
        'cancelBlock' => \app\transactions\CancelBlock::class,
        'executeBlock' => \app\transactions\ExecuteBlock::class,
    ]
];