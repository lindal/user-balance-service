<?php

namespace app;


use app\errors\InvalidTransactionType;
use app\interfaces\ITransaction;

class TransactionFactory
{

    /**
     * @var \PDO
     */
    private $_pdo;

    /**
     * Available transaction types
     */
    private $_types = [];

    /**
     * TransactionFactory constructor.
     * @param \PDO $pdo
     * @param array $types
     */
    public function __construct(\PDO $pdo, array $types)
    {
        $this->_pdo = $pdo;
        $this->_types = $types;
    }

    /**
     * Create new transaction object depends on type
     * @param string $type
     * @param array $data
     * @return ITransaction
     * @throws InvalidTransactionType
     */
    public function make(string $type, array $data): ITransaction
    {
        if (!isset($this->_types[$type])) {
            throw new InvalidTransactionType();
        }
        $className = $this->_types[$type];
        return new $className(new TransactionRepository($this->_pdo), $data);
    }

}