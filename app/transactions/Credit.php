<?php

namespace app\transactions;


use app\errors\NotEnoughBalance;
use app\interfaces\ITransaction;

class Credit extends AbstractTransaction implements ITransaction
{

    const CODE = 2;
    const TYPE = 2;

    /**
     * @inheritdoc
     */
    public function execute()
    {
        if (!$this->isEnoughBalance($this->_data['user_id'], $this->_data['value'])) {
            throw new NotEnoughBalance();
        }

        $this->addTransactionToDb(
            (int)$this->_data['user_id'],
            (int)$this->_data['value'],
            self::TYPE
        );
    }
}