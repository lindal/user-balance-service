<?php

namespace app\transactions;

use app\TransactionRepository;

abstract class AbstractTransaction
{

    const TYPE_DEBIT = 1;
    const TYPE_CREDIT = 2;
    const TYPE_BLOCK = 3;
    const STATUS_BLOCKED = 1;
    const STATUS_EXECUTED = 2;
    protected $_data = [];

    /**
     * @var TransactionRepository
     */
    protected $_repository;

    /**
     * AbstractTransaction constructor.
     * @param TransactionRepository $repository
     * @param array $data
     */
    public function __construct(TransactionRepository $repository, array $data)
    {
        $this->_data = $data;
        $this->_repository = $repository;
    }

    /**
     * Default validation
     * @return bool
     */
    public function validate(): bool
    {
        $valid = true;
        $valid &= isset($this->_data['value']) && is_numeric($this->_data['value']);
        $valid &= isset($this->_data['user_id']) && is_numeric($this->_data['user_id']);
        return $valid;
    }

    /**
     * @param int $userId
     * @param int $value
     * @param int $type
     * @param int $status
     * @return int Transaction id
     */
    protected function addTransactionToDb(int $userId, int $value, int $type, int $status = self::STATUS_EXECUTED): int
    {
        return $this->_repository
            ->insert([
                'user_id' => $userId,
                'value' => $value,
                'type' => $type,
                'status' => $status
            ]);
    }

    /**
     * @param int $userId
     * @param int $value
     * @return bool
     */
    protected function isEnoughBalance(int $userId, int $value): bool
    {
        $balance = $this->_repository->getUserBalance($userId);

        if ($balance < $value) {
            return false;
        }

        return true;
    }

}