<?php

namespace app\transactions;


use app\errors\DbException;
use app\errors\NotFound;
use app\interfaces\ITransaction;

class ExecuteBlock extends AbstractTransaction implements ITransaction
{

    /**
     * @inheritdoc
     */
    public function validate(): bool
    {
        $valid = true;
        $valid &= isset($this->_data['transaction_id']) && is_numeric($this->_data['transaction_id']);
        return $valid;
    }

    /**
     * @inheritdoc
     */
    public function execute()
    {
        try {
            $this->_repository->getPdo()->beginTransaction();

            $data = $this->_repository->findBlockedTransaction($this->_data['transaction_id']);
            if (!$data) {
                throw new NotFound();
            }

            $this->addTransactionToDb($data['user_id'], $data['value'], self::TYPE_CREDIT);

            $this->_repository->update($data['id'], ['status' => self::STATUS_EXECUTED]);

            $this->_repository->getPdo()->commit();
        } catch (NotFound $e) {
            throw $e;
        } catch (\Exception $e) {
            $this->_repository->getPdo()->rollBack();
            throw new DbException();
        }
    }
}