<?php

namespace app\transactions;


use app\errors\NotEnoughBalance;
use app\interfaces\ITransaction;

class Block extends AbstractTransaction implements ITransaction
{

    /**
     * Execute transaction
     */
    public function execute()
    {
        if (!$this->isEnoughBalance($this->_data['user_id'], $this->_data['value'])) {
            throw new NotEnoughBalance();
        }

        $this->addTransactionToDb(
            $this->_data['user_id'],
            $this->_data['value'],
            self::TYPE_BLOCK,
            self::STATUS_BLOCKED
        );
    }
}