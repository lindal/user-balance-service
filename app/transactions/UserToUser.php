<?php

namespace app\transactions;


use app\errors\DbException;
use app\errors\NotEnoughBalance;
use app\interfaces\ITransaction;

class UserToUser extends AbstractTransaction implements ITransaction
{

    /**
     * @inheritdoc
     */
    public function validate(): bool
    {
        $valid = true;
        $valid &= isset($this->_data['value']) && is_numeric($this->_data['value']);
        $valid &= isset($this->_data['sender_id']) && is_numeric($this->_data['sender_id']);
        $valid &= isset($this->_data['recipient_id']) && is_numeric($this->_data['recipient_id']);
        return $valid;
    }

    /**
     * @inheritdoc
     */
    public function execute()
    {
        try {
            $this->_repository->getPdo()->beginTransaction();

            if (!$this->isEnoughBalance($this->_data['sender_id'], $this->_data['value'])) {
                throw new NotEnoughBalance();
            }

            $this->addTransactionToDb(
                (int)$this->_data['sender_id'],
                (int)$this->_data['value'],
                self::TYPE_CREDIT
            );
            $this->addTransactionToDb(
                (int)$this->_data['recipient_id'],
                (int)$this->_data['value'],
                self::TYPE_DEBIT
            );

            $this->_repository->getPdo()->commit();
        } catch (NotEnoughBalance $e) {
            throw $e;
        } catch (\Exception $e) {
            $this->_repository->getPdo()->rollBack();
            throw new DbException();
        }
    }
}