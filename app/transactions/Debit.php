<?php

namespace app\transactions;


use app\interfaces\ITransaction;

class Debit extends AbstractTransaction implements ITransaction
{

    /**
     * @inheritdoc
     */
    public function execute()
    {
        $this->addTransactionToDb(
            (int)$this->_data['user_id'],
            (int)$this->_data['value'],
            self::TYPE_DEBIT
        );
    }
}