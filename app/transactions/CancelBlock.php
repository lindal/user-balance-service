<?php

namespace app\transactions;


use app\errors\NotFound;
use app\interfaces\ITransaction;

class CancelBlock extends AbstractTransaction implements ITransaction
{

    const CODE = 4;

    /**
     * @inheritdoc
     */
    public function validate(): bool
    {
        $valid = true;
        $valid &= isset($this->_data['transaction_id']) && is_numeric($this->_data['transaction_id']);
        return $valid;
    }

    /**
     * @inheritdoc
     */
    public function execute()
    {
        $data = $this->_repository->findBlockedTransaction($this->_data['transaction_id']);
        if (!$data) {
            throw new NotFound();
        }

        $this->_repository->update($data['id'], ['status' => self::STATUS_EXECUTED]);
    }
}