<?php

namespace app\errors;


class InvalidTransactionType extends \Exception
{

    protected $message = 'Unavailable transaction type';

}