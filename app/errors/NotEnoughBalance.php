<?php

namespace app\errors;


class NotEnoughBalance extends \Exception
{

    protected $message = 'Not enough money for this transaction.';

}