<?php

namespace app\errors;


class DbException extends \Exception
{

    protected $message = 'Transaction has not saved!';

}