<?php

namespace app\errors;


class ValidationException extends \Exception
{

    protected $message = 'Message has invalid data';

}