<?php
/**
 * Created by PhpStorm.
 * User: lindal
 * Date: 22.09.17
 * Time: 19:53
 */

namespace app\errors;


class NotFound extends \Exception
{
    protected $message = 'Transaction not found';
}