<?php

namespace app;


use app\transactions\AbstractTransaction;

class TransactionRepository
{

    /**
     * @var \PDO
     */
    private $_pdo;

    public function __construct(\PDO $pdo)
    {
        $this->_pdo = $pdo;
    }

    /**
     * @return \PDO
     */
    public function getPdo(): \PDO
    {
        return $this->_pdo;
    }


    /**
     * @param array $data
     * @return int Last insetrted id
     */
    public function insert(array $data): int
    {
        $fields = array_keys($data);
        $values = [];
        foreach ($data as $key => $value) {
            $values[':' . $key] = $value;
        }
        $sql = "INSERT INTO `transaction` (" . implode(', ', $fields) . ") VALUES (" . implode(', ', array_keys($values)) . ")";
        $this->_pdo->prepare($sql)->execute($values);
        return $this->_pdo->lastInsertId();
    }

    /**
     * @param int $userId
     * @return int
     */
    public function getUserBalance(int $userId): int
    {
        $sql = "SELECT (IFNULL(SUM(debit.value), 0) - IFNULL(SUM(credit.value), 0) - IFNULL(SUM(block.value), 0)) as balance 
          FROM `transaction` main 
          LEFT JOIN `transaction` debit ON main.id = debit.id AND debit.type = :debitType
          LEFT JOIN `transaction` credit ON main.id = credit.id AND credit.type = :creditType
          LEFT JOIN `transaction` block ON main.id = block.id AND block.type = :blockType AND block.status = :blockStatus
          WHERE main.user_id = :userId
          GROUP BY main.user_id";
        $stmt = $this->_pdo->prepare($sql);
        $stmt->execute([
            ':userId' => $userId,
            ':debitType' => AbstractTransaction::TYPE_DEBIT,
            ':creditType' => AbstractTransaction::TYPE_CREDIT,
            ':blockType' => AbstractTransaction::TYPE_BLOCK,
            ':blockStatus' => AbstractTransaction::STATUS_BLOCKED
        ]);
        return (int)$stmt->fetchColumn();
    }

    /**
     * @param int $id
     * @return array
     */
    public function findById(int $id): array
    {
        $sql = "SELECT * FROM `transaction` WHERE id = :id";
        $stmt = $this->_pdo->prepare($sql);
        $stmt->execute([':id' => $id]);
        return $stmt->fetch() ?: [];
    }

    /**
     * @param int $id
     * @return array
     */
    public function findBlockedTransaction(int $id): array
    {
        $sql = "SELECT * FROM `transaction` WHERE id = :id AND type = :type AND status = :status";
        $stmt = $this->_pdo->prepare($sql);
        $stmt->execute([
            ':id' => $id,
            ':type' => AbstractTransaction::TYPE_BLOCK,
            ':status' => AbstractTransaction::STATUS_BLOCKED,
        ]);
        return $stmt->fetch() ?: [];
    }

    /**
     * @param int $id
     * @param $data
     */
    public function update(int $id, $data)
    {
        $values = [];
        $set = [];
        foreach ($data as $key => $value) {
            $values[':' . $key] = $value;
            $set[] = $key . ' = :' . $key;
        }

        $sql = "UPDATE `transaction` SET " . implode(', ', $set) . " WHERE id = :id";
        $values[':id'] = $id;
        $stmt = $this->_pdo->prepare($sql);
        $stmt->execute($values);
    }

}