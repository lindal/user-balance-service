<?php

namespace app\interfaces;

interface ITransaction
{

    /**
     * Validation transaction's data
     * @return bool
     */
    public function validate(): bool;

    /**
     * Execute transaction
     */
    public function execute();

}