<?php

namespace app;

use app\errors\ValidationException;
use PDO;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class App
{

    /**
     * @var AMQPStreamConnection
     */
    private $_connection;
    private $_channel;
    private $_config;

    /**
     * @var TransactionFactory
     */
    private $_transactionFactory;

    /**
     * @var PDO
     */
    private $_pdo;

    /**
     * App constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->_config = $config;
        $this->initConnection();
        $this->initChannel();
        $this->initPdo();
        $this->_transactionFactory = new TransactionFactory($this->_pdo, $config['availableTransactionTypes']);
    }

    /**
     * Run worker
     */
    public function run()
    {
        while (count($this->_channel->callbacks)) {
            $this->_channel->wait();
        }
        $this->_channel->close();
        $this->_connection->close();
    }

    /**
     * Initializing RabbitMQ connection
     */
    private function initConnection()
    {
        $this->_connection = new AMQPStreamConnection(
            $this->_config['broker']['host'],
            $this->_config['broker']['port'],
            $this->_config['broker']['user'],
            $this->_config['broker']['password']
        );
    }

    /**
     * Initializing PDO
     */
    private function initPdo()
    {
        $this->_pdo = new PDO(
            $this->_config['pdo']['dsn'],
            $this->_config['pdo']['user'],
            $this->_config['pdo']['password'],
            [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES => false,
            ]
        );
    }

    /**
     * Initializing channel
     */
    private function initChannel()
    {
        $this->_channel = $this->_connection->channel();
        $this->_channel->queue_declare($this->_config['broker']['queue'], false, false, false, false);
        $this->_channel->basic_qos(null, 1, null);
        $this->_channel->basic_consume($this->_config['broker']['queue'], '', false, false, false, false, [$this, 'channelCallback']);
    }

    /**
     * @param AMQPMessage $msg
     */
    public function channelCallback($msg)
    {
        try {
            $data = json_decode($msg->getBody(), true);
            $type = $data['transaction_type'] ?? '';
            $transaction = $this->_transactionFactory->make($type, $data);
            if (!$transaction->validate()) {
                throw new ValidationException();
            }
            $transaction->execute();
            $response = [
                'success' => true,
                'message' => 'Transaction was recorded'
            ];
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }
        $response = json_encode($response);
        $resMsg = new AMQPMessage(
            $response,
            ['correlation_id' => $msg->get('correlation_id')]
        );
        $msg->delivery_info['channel']->basic_publish($resMsg, '', $msg->get('reply_to'));
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    }

}