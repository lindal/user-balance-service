<?php

require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class BrokerTest
{
    private $connection;
    private $channel;
    private $callback_queue;
    private $response;
    private $corr_id;

    public function __construct()
    {
        $this->connection = new AMQPStreamConnection(
            '0.0.0.0', 5672, 'rabbitmq', 'rabbitmq');
        $this->channel = $this->connection->channel();
        list($this->callback_queue, ,) = $this->channel->queue_declare(
            "", false, false, true, false);
        $this->channel->basic_consume(
            $this->callback_queue, '', false, false, false, false,
            array($this, 'on_response'));
    }

    public function on_response($rep)
    {
        if ($rep->get('correlation_id') == $this->corr_id) {
            $this->response = $rep->body;
        }
    }

    public function call($data)
    {
        $this->response = null;
        $this->corr_id = uniqid();

        $msg = new AMQPMessage(
            (string)json_encode($data),
            array('correlation_id' => $this->corr_id,
                'reply_to' => $this->callback_queue)
        );
        $this->channel->basic_publish($msg, '', 'rpc_queue');
        while (!$this->response) {
            $this->channel->wait();
        }
        return $this->response;
    }
}


$test = new BrokerTest();

$types = [
    'debit', 'credit', 'block', 'userToUser', 'cancelBlock', 'executeBlock'
];

for ($i = 0; $i < 100; $i++) {
    $type = $types[mt_rand(0, 5)];
    $userId = mt_rand(1, 1);
    $value = mt_rand(1, 1000);
    if (in_array($type, ['debit', 'credit', 'block'])) {
        $data = [
            'transaction_type' => $type,
            'user_id' => $userId,
            'value' => $value
        ];
    } elseif (in_array($type, ['cancelBlock', 'executeBlock'])) {
        $data = [
            'transaction_type' => $type,
            'transaction_id' => mt_rand(2, 100),
        ];
    } else {
        $data = [
            'transaction_type' => $type,
            'sender_id' => $userId,
            'recipient_id' => 2,
            'value' => $value
        ];
    }
    $response = $test->call($data);
    $response = json_decode($response, true);
    echo " [" . ($response['success'] ? "." : "!") . "] Got ", ($response['message'] ?? 'Something wrong'), "\n";
}

