<?php

class TransactionRepositoryTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \app\TransactionRepository
     */
    public $model;

    /**
     * @var PDO
     */
    private $_pdo;

    public function setUp()
    {
        $pdo = new PDO('mysql:host=db;dbname=dev;charset=utf8', 'root', 123456);
        $stmt = $pdo->prepare('TRUNCATE `transaction`');
        $stmt->execute();
        $this->_pdo = $pdo;
        $this->model = new \app\TransactionRepository($pdo);
    }

    private function compareData($data1, $data2)
    {
        foreach ($data1 as $key => $value) {
            if (isset($data2[$key])) {
                $this->assertEquals($data2[$key], $data1[$key]);
            }
        }
    }

    /**
     * @dataProvider insertDataProvider
     * @param $data
     */
    public function testInsert($data)
    {
        $id = $this->model->insert($data);
        $this->assertTrue(is_numeric($id));
        $row = $this->model->findById($id);
        $this->compareData($row, $data);
    }

    public function insertDataProvider()
    {
        return [
            [['user_id' => 1, 'value' => 100, 'status' => 2, 'type' => 1]],
            [['user_id' => 1, 'value' => 50, 'status' => 2, 'type' => 2]],
            [['user_id' => 1, 'value' => 50, 'status' => 2, 'type' => 3]],
            [['user_id' => 1, 'value' => 50, 'status' => 1, 'type' => 3]],
            [['user_id' => 2, 'value' => 50, 'status' => 2, 'type' => 2]],
        ];
    }

    /**
     * @dataProvider insertDataProvider
     * @param $data
     */
    public function testUpdate($data)
    {
        $id = $this->model->insert($data);
        $newStatus = 3;
        $this->model->update($id, ['status' => $newStatus]);
        $row = $this->model->findById($id);
        $data['status'] = $newStatus;
        $this->compareData($row, $data);
    }

    /**
     * @dataProvider balanceDataProvider
     * @param $transactions
     * @param $userId
     * @param $expected
     */
    public function testGetUserBalance($transactions, $userId, $expected)
    {
        foreach ($transactions as $transaction) {
            $this->model->insert($transaction);
        }
        $balance = $this->model->getUserBalance($userId);
        $this->assertEquals($expected, $balance);
    }

    public function balanceDataProvider()
    {
        return [
            [
                [
                    ['user_id' => 1, 'value' => 100, 'status' => 2, 'type' => 1],
                    ['user_id' => 1, 'value' => 50, 'status' => 2, 'type' => 2],
                    ['user_id' => 2, 'value' => 50, 'status' => 2, 'type' => 1],
                    ['user_id' => 2, 'value' => 50, 'status' => 2, 'type' => 2],
                    ['user_id' => 1, 'value' => 25, 'status' => 1, 'type' => 3],
                    ['user_id' => 1, 'value' => 25, 'status' => 2, 'type' => 3],
                ],
                1,
                25
            ],
        ];
    }

    /**
     * @dataProvider insertDataProvider
     * @param $data
     */
    public function testFindBlockedTransaction($data)
    {
        $id = $this->model->insert($data);
        if (!($data['type'] == 3 && $data['status'] == 1)) {
            $this->assertTrue(true);
            return;
        }
        $expected = $data;
        $row = $this->model->findBlockedTransaction($id);
        $this->compareData($row, $expected);
    }

}