<?php

/**
 * Created by PhpStorm.
 * User: lindal
 * Date: 25.09.17
 * Time: 13:36
 */
class TransactionFactoryTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var \app\TransactionFactory
     */
    public $factory;

    public function setUp()
    {
        $pdo = $this->createMock(PDO::class);
        $types = [
            'debit' => \app\transactions\Debit::class,
            'credit' => \app\transactions\Credit::class
        ];
        $this->factory = new \app\TransactionFactory($pdo, $types);
    }

    public function testMake()
    {
        $obj = $this->factory->make('debit', []);
        $this->assertInstanceOf(\app\transactions\Debit::class, $obj);
        $obj = $this->factory->make('credit', []);
        $this->assertInstanceOf(\app\transactions\Credit::class, $obj);
    }

    /**
     * @expectedException \app\errors\InvalidTransactionType
     */
    public function testMakeWithWrongType()
    {
        $obj = $this->factory->make('wrongType', []);
    }

}