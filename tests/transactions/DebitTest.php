<?php

class DebitTest extends \PHPUnit\Framework\TestCase
{

    public $repository;

    public function setUp()
    {
        $this->repository = $this->createMock(\app\TransactionRepository::class);
        $this->repository->method('insert')->willReturn(1);
    }

    /**
     * @dataProvider validateProvider
     * @param $data
     * @param $expected
     */
    public function testValidate($data, $expected)
    {
        $model = new \app\transactions\Debit($this->repository, $data);
        $this->assertEquals($model->validate(), $expected);
    }

    public function validateProvider()
    {
        return [
            [[], false],
            [['user_id' => 'fasd', 'value' => 'fasd'], false],
            [['user_id' => 'fasd', 'value' => 213], false],
            [['user_id' => 4123, 'value' => 213], true],
        ];
    }

}