<?php

class CreditTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @dataProvider dataProvider
     * @param $value
     * @param $currentBalance
     * @param $expectException
     */
    public function testExecute($value, $currentBalance, $expectException)
    {
        $repository = $this->createMock(\app\TransactionRepository::class);
        $repository->method('getUserBalance')->willReturn($currentBalance);
        $repository->method('insert')->willReturn(1);
        $model = new \app\transactions\Credit($repository, ['user_id' => 1, 'value' => $value]);
        if ($expectException) {
            $this->expectException(\app\errors\NotEnoughBalance::class);
        }
        $model->execute();
        $this->assertTrue(true);
    }

    public function dataProvider()
    {
        return [
            [100, 50, true],
            [100, 100, false],
            [100, 150, false],
        ];
    }


    /**
     * @dataProvider validateProvider
     * @param $data
     * @param $expected
     */
    public function testValidate($data, $expected)
    {
        $repository = $this->createMock(\app\TransactionRepository::class);
        $repository->method('insert')->willReturn(1);
        $model = new \app\transactions\Debit($repository, $data);
        $this->assertEquals($model->validate(), $expected);
    }

    public function validateProvider()
    {
        return [
            [[], false],
            [['user_id' => 'fasd', 'value' => 'fasd'], false],
            [['user_id' => 'fasd', 'value' => 213], false],
            [['user_id' => 4123, 'value' => 213], true],
        ];
    }

}