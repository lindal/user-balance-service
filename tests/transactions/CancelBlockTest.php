<?php

/**
 * Created by PhpStorm.
 * User: lindal
 * Date: 25.09.17
 * Time: 12:44
 */
class CancelBlockTest extends \PHPUnit\Framework\TestCase
{


    /**
     * @dataProvider dataProvider
     * @param $mockData
     * @param $notFound
     */
    public function testExecute($mockData, $notFound)
    {
        $repository = $this->createMock(\app\TransactionRepository::class);
        $repository->method('findBlockedTransaction')->willReturn($mockData);
        $repository->method('update');
        $model = new \app\transactions\CancelBlock($repository, ['transaction_id' => 1]);
        if ($notFound) {
            $this->expectException(\app\errors\NotFound::class);
        }
        $model->execute();
        $this->assertTrue(true);
    }

    public function dataProvider()
    {
        return [
            [[], true],
            [['id' => 1, 'user_id' => 1, 'value' => 100, 'type' => 3, 'status' => 1], false]
        ];
    }


    /**
     * @dataProvider validateProvider
     * @param $data
     * @param $expected
     */
    public function testValidate($data, $expected)
    {
        $repository = $this->createMock(\app\TransactionRepository::class);
        $repository->method('insert')->willReturn(1);
        $model = new \app\transactions\CancelBlock($repository, $data);
        $this->assertEquals($model->validate(), $expected);
    }

    public function validateProvider()
    {
        return [
            [[], false],
            [['transaction_id' => 'fasd'], false],
            [['transaction_id' => 1], true],
        ];
    }

}