<?php

class UserToUserTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @dataProvider validateProvider
     * @param $data
     * @param $expected
     */
    public function testValidate($data, $expected)
    {
        $repository = $this->createMock(\app\TransactionRepository::class);
        $repository->method('insert')->willReturn(1);
        $model = new \app\transactions\UserToUser($repository, $data);
        $this->assertEquals($model->validate(), $expected);
    }

    public function validateProvider()
    {
        return [
            [[], false],
            [['sender_id' => 'fasd', 'recipient_id' => 'fasd', 'value' => 'fasd'], false],
            [['sender_id' => 'fasd', 'recipient_id' => 'fasd', 'value' => 321], false],
            [['sender_id' => 'fds', 'recipient_id' => 2, 'value' => 423], false],
            [['sender_id' => 1, 'recipient_id' => 2, 'value' => 423], true],
        ];
    }


    /**
     * @dataProvider dataProvider
     * @param $value
     * @param $currentBalance
     * @param $expectException
     */
    public function testExecute($value, $currentBalance, $expectException)
    {
        $repository = $this->createMock(\app\TransactionRepository::class);
        $repository->method('getUserBalance')->willReturn($currentBalance);
        $repository->method('insert')->willReturn(1);
        $model = new \app\transactions\UserToUser($repository, ['sender_id' => 1, 'recipient_id' => 2, 'value' => $value]);
        if ($expectException) {
            $this->expectException(\app\errors\NotEnoughBalance::class);
        }
        $model->execute();
        $this->assertTrue(true);
    }

    public function dataProvider()
    {
        return [
            [100, 50, true],
            [100, 100, false],
            [100, 150, false],
        ];
    }
}